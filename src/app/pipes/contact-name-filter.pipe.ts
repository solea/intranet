import {Injectable, Pipe, PipeTransform} from '@angular/core';
import { Contact } from 'src/app/models/contact.model';
import { FilterOptions } from 'src/app/models/filter-options.model';

@Pipe({
    name: 'contactNameFilterPipe',
    pure: false
})

@Injectable()
export class ContactNameFilterPipe implements PipeTransform {

  /* Retourne un tableau de contacts correspondant à le recherche (key) filtrée sur les critères (options)*/
  transform(contacts: Contact[], args?: Array<object>): any {
    
    let contactsResult: Contact[];
    let key = args[0] as String;
    let options = args[1] as FilterOptions;
    let user= args[2] as Contact;

    contactsResult = new Array(0);
    
    /* Gère le champs recherche non défini */
    if (args[0] === undefined){
      key = "";
    }

    if (options.onCity){
      contactsResult = contactsResult.concat(contacts.filter(contacts => contacts.city.toLowerCase().indexOf(key.toLowerCase()) !== -1));
    }
    if (options.onFunction){
      contactsResult = contactsResult.concat(contacts.filter(contacts => contacts.job.toLowerCase().indexOf(key.toLowerCase()) !== -1));
    }
    if (options.onMail){
      contactsResult = contactsResult.concat(contacts.filter(contacts => contacts.mail.toLowerCase().indexOf(key.toLowerCase()) !== -1));
    }
    if (options.onName){
      contactsResult = contactsResult.concat(contacts.filter(contacts => contacts.name.toLowerCase().indexOf(key.toLowerCase()) !== -1));
    }
    if (options.onNickName){
      contactsResult = contactsResult.concat(contacts.filter(contacts => contacts.surname.toLowerCase().indexOf(key.toLowerCase()) !== -1));
    }
    if (options.onPhone){
      contactsResult = contactsResult.concat(contacts.filter(contacts => contacts.phone.toLowerCase().indexOf(key.toLowerCase()) !== -1));
    }

    // Suprime de la liste de recherche l'utilisateur actuellement connecté
    contactsResult = contactsResult.filter(contacts => contacts.id !== user.id); 

    return uniq(contactsResult)
      .sort(
        function(contact1, contact2)
        {
          var name1 = contact1.name.toLowerCase();
          var name2 = contact2.name.toLowerCase();
          if (name1 < name2) //sort string ascending
              return -1;
          if (name1 > name2)
              return 1;
          return 0;//default return value (no sorting)
        }
      )
    ;

    function uniq(contacts:Contact[]) {
      return Array.from(new Set(contacts));
   }
  }
}
