import { Pipe, PipeTransform } from '@angular/core';
import { FilterOptions } from 'src/app/models/filter-options.model';

@Pipe({
  name: 'labelFilterPipe'
})
export class LabelFilterPipePipe implements PipeTransform {

  transform(options: FilterOptions): string {
    let count: number;
    let label: string;

    count = 0;
    
    if (options.onCity) {count = count+1; label = "Site"}
    if (options.onFunction) {count = count+1; label = "Fonction"}
    if (options.onMail) {count = count+1; label = "Mail"}
    if (options.onName) {count = count+1; label = "Nom"}
    if (options.onNickName) {count = count+1; label = "Prénom"}
    if (options.onPhone) {count = count+1; label = "Téléphone"}

    if (count == 0){
      label = "Aucuns";
    }

    if (count > 1){
      label = String(count);
    }

    if (count == 6){
      label = "Tous";
    }

    return label;
  }

}
