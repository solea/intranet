import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { INotification } from './interfaces/INotification.interface';
import { Subscription } from 'rxjs';
import { NotificationsService } from './services/notifications.service';
import { NotificationsDirective } from './directives/notifications.directive';
import { NotificationItem } from './components/notifications/notification-item';
import { NotificationsComponent } from './components/notifications/notifications.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(NotificationsDirective) notificationsHost: NotificationsDirective;

  brand = 'Solea';

  notifications: NotificationItem[];
  notificationsSubscription: Subscription;

  constructor(
    private notificationsService: NotificationsService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {
    // Souscriptions
    this.notificationsSubscription = this.notificationsService.notificationsSubject.subscribe(
      (notifications: NotificationItem[]) => {
        this.notifications = notifications;
      }
    );
    this.notificationsService.emitNotifications();

    this.loadNotifications();
  }

  ngOnDestroy() {
    this.notificationsSubscription.unsubscribe();
  }

  createNotification(notification: INotification){
    this.notificationsService.createNotification(notification);
    this.loadNotifications();
  }

  clearNotifications(){
    let viewContainerRef = this.notificationsHost.viewContainerRef;
    viewContainerRef.clear();
  }

  private loadNotifications() {
    this.clearNotifications();

    let viewContainerRef = this.notificationsHost.viewContainerRef;

    this.notifications = this.notificationsService.getNotifications();

    this.notifications.forEach(notificationItem => {
      let componentFactory = this.componentFactoryResolver.resolveComponentFactory(notificationItem.component);
      let componentRef = viewContainerRef.createComponent(componentFactory);
      (<NotificationsComponent>componentRef.instance).type = notificationItem.notification.type;
      (<NotificationsComponent>componentRef.instance).title = notificationItem.notification.title;
      (<NotificationsComponent>componentRef.instance).content = notificationItem.notification.content;
    });
  }
}
