import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[notifications-host]'
})
export class NotificationsDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
