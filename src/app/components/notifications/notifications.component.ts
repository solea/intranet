import { Component, Input } from '@angular/core';
import { INotification } from 'src/app/interfaces/INotification.interface';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})

export class NotificationsComponent implements INotification  {
  @Input() type: string;
  @Input() title: string;
  @Input() content: string;
}
