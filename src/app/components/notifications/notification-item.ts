import { Type } from '@angular/core';
import { INotification } from 'src/app/interfaces/INotification.interface';

export class NotificationItem {
  constructor(public component: Type<any>, public notification: INotification) {}
}