import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ContactService } from 'src/app/services/contact.service';
import { Contact } from 'src/app/models/contact.model';
import { Subscription } from 'rxjs';
import { AppComponent } from 'src/app/app.component';
import { HttpErrorResponse } from '@angular/common/http';
import { PhotosService } from 'src/app/services/photos.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  user: Contact;
  userSubscription: Subscription;

  editForm: FormGroup;

  selectedFile: ImageSnippet;
  imageLabel: string;

  // Tous les groupes utilisateurs du LDAP
  groupsSubscription: Subscription;
  groups: any[];
  groupsModel: any[];

  constructor(
    private appComponent: AppComponent,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private contactService: ContactService,
    private photoService: PhotosService,
    private router: Router
  ) { }

  async ngOnInit() {
    // Souscriptions
    this.userSubscription = this.authService.loggedUserSubject.subscribe(
      (contact: Contact) => {
        this.user = contact;
      }
    );
    this.authService.emitLoggedUser();

    this.groupsSubscription = this.contactService.groupsSubject.subscribe(
      (groups: any[]) => {
        this.groups = groups;
      }
    );

    if (!this.groups) {
      await this.contactService.getGroups();
    }
    this.contactService.emitGroups();

    var groupMember = [];
    this.groups.forEach(group => {
      if (this.user.groups.find(grp => grp.guid == group.guid)) {
        groupMember.push({ guid: group.guid, gname: group.gname, isMember: true });
      } else {
        groupMember.push({ guid: group.guid, gname: group.gname, isMember: false });
      }
    });

    this.groupsModel = groupMember;

    this.initForm();

    this.imageLabel = "Aucun fichier choisi";
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  initForm() {
    this.editForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(/[a-zA-Z]/)]],
      surname: ['', [Validators.required, Validators.pattern(/[a-zA-Z]/)]],
      job: [''],
      city: [''],
      phone: [''],
      groups: new FormArray([]),
      currentPassword: [''],
      newPassword: ['', [Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      confirmNewPassword: ['']
    });

    this.addGroups();
  }

  private addGroups() {
    this.groupsModel.map((group, i) => {
      const control = new FormControl(group["isMember"]);
      (this.editForm.controls.groups as FormArray).push(control);
    });
  }

  /* Edition du profil */
  onSubmit() {
    let name = this.editForm.get('name').value;
    let surname = this.editForm.get('surname').value;
    let job = this.editForm.get('job').value;
    let city = this.editForm.get('city').value;
    let phone = this.editForm.get('phone').value;
    let currentPassword = this.editForm.get('currentPassword').value;
    let newPassword = this.editForm.get('newPassword').value;
    let groups = this.editForm.get('groups').value;

    /* Garde fou, on réinitialise les champs vide aux valeurs par défaut */
    if (name === "") {
      name = this.user.name;
    }
    if (surname === "") {
      surname = this.user.surname;
    }
    if (job === "") {
      job = this.user.job;
    }
    if (city == "") {
      city = this.user.city;
    }
    if (phone === "") {
      phone = this.user.phone;
    }

    for (var i = 0; i < this.groupsModel.length; i++) {
      this.groupsModel[i].isMember = groups[i];
    }

    var contact = {
      id: this.user.id,
      uid: this.user.uid,
      name: name,
      surname: surname,
      job: job,
      city: city,
      phone: phone,
      mobile: '',
      mail: this.user.mail,
      isAdmin: this.user.isAdmin,
      photo: this.user.photo,
      groups: this.groupsModel
    }

    this.contactService.editContact(contact, false)
      .then(
        () => {
          if (newPassword != "" && currentPassword != "") {
            this.contactService.changePassword(currentPassword, newPassword)
              .then(
                () => {
                  this.authService.signOut().then( // -> On déconnecte
                    () => {
                      this.appComponent.createNotification({ type: "warning", title: "Mot de passe mis à jour", content: "Merci de vous reconnecter avec votre nouveau mot de passe." });
                      this.router.navigate(['/auth']);
                    },
                    (erro) => {
                      this.handleErrors(erro);
                    }
                  );
                },
                (err) => {
                  this.handleErrors(err);
                }
              );
          } else {
            this.authService.updateUser().then(
              () => {
                this.appComponent.createNotification({ type: "success", title: "Informations personnelles mises à jour", content: "" });
                this.router.navigate(['/dashboard/annuaire']);
              },
              (erro) => {
                this.handleErrors(erro);
              }
            );
          }
        },
        (error) => {
          this.handleErrors(error);
        }
      );
  }

  onImageChanged(imageInput: any) {
    const selectedImage = imageInput.files[0];
    this.imageLabel = selectedImage.name;
    
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {

      this.selectedFile = new ImageSnippet(event.target.result, selectedImage);

      this.photoService.uploadImage(this.selectedFile.file).then(
        () =>  {
          this.authService.updateUser().then(
            () => {
              this.appComponent.createNotification({ type: "success", title: "Photo ajoutée", content: "Vous avez défini une nouvelle photo de profil. Si l'image ne s'affiche pas, vous pouvez tenter de rafraîchir la page (F5)." });
            }
            , (err) => {
              this.handleErrors(err);
            }
          );
        },
        (error) => {
          this.handleErrors(error);
        }
      );
    });

    reader.readAsDataURL(selectedImage);
  }

  onImageUploaded() {
    /* TODO gérer l'import d'images */
  }

  deletePhoto() {

    if (this.user.photo != '') {
      this.photoService.removePhoto().then(
        () => {

          this.authService.updateUser().then(
            () => {
              this.appComponent.createNotification({ type: "success", title: "Photo retirée", content: "Vous avez retiré votre photo de profil." });
            }
            , (err) => {
              this.handleErrors(err);
            }
          );
        },
        (error) => {
          this.handleErrors(error);
        }
      );
    }
  }

  handleErrors(error) {
    if (error instanceof HttpErrorResponse && error.status == 401) {
      this.appComponent.createNotification({ type: "warning", title: "Session expirée", content: "Votre session a expirée, merci de vous reconnecter." });
      this.router.navigate(['/auth']);
    } else {
      this.appComponent.createNotification({ type: "danger", title: "Erreur", content: error.error.message });
    }
  }
}


class ImageSnippet {
  constructor(public src: string, public file: File) {}
}