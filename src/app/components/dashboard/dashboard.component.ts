import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Contact } from '../../models/contact.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user : Contact;
  userSubscription: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    // Souscriptions
    this.userSubscription = this.authService.loggedUserSubject.subscribe(
      (contact: Contact) => {
        this.user = contact;
      }
    );
    this.authService.emitLoggedUser();
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  onSignOut(){

    this.authService.signOut().then(
      () => {
        console.log("Déconnexion effectuée.");
      },
      (err) => {
        console.log("Erreur :" + err);
      }
    );
  }
}