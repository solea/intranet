import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Contact } from 'src/app/models/contact.model';
import { FilterOptions } from 'src/app/models/filter-options.model';
import { Subscription } from 'rxjs/Subscription';
import { ContactService } from 'src/app/services/contact.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppComponent } from 'src/app/app.component';
import { HttpErrorResponse } from '@angular/common/http';
import { PhotosService } from 'src/app/services/photos.service';
import { VoipService } from 'src/app/services/voip.service';

@Component({
  selector: 'app-annuaire',
  templateUrl: './annuaire.component.html',
  styleUrls: ['./annuaire.component.scss']
})
export class AnnuaireComponent implements OnInit {

  // Utilisateur actuellement connecté
  user : Contact;
  userSubscription: Subscription;
  
  // Tous les utilisateurs du LDAP
  contacts: Contact[];
  contactsSubscription: Subscription;
  
  // Tous les groupes utilisateurs du LDAP
  groupsSubscription: Subscription;
  groups: any[];

  // Critères de recherche
  searchKey: string;
  searchOptions: FilterOptions;
  selectLabel: string;
  searchAll: boolean;

  // Affichage des erreurs sur opérations
  errorMessage: string;
  isError: boolean;

  // Contact sélectionné pour les opérations CRUD
  contactModel: Contact;
  resetPassword: boolean;
  resetPhoto: boolean;
  
  constructor(
    private appComponent: AppComponent,
    private authService: AuthService, 
    private contactsService: ContactService,
    private photoService: PhotosService,
    private voipService: VoipService,
    private router: Router
  ) { }

  ngOnInit() {
    // Souscriptions
    this.userSubscription = this.authService.loggedUserSubject.subscribe(
      (loggedUser: Contact) => {
        this.user = loggedUser;
      }
    );
    this.authService.emitLoggedUser();
    
    this.contactsSubscription = this.contactsService.contactsSubject.subscribe(
      (contacts: Contact[]) => {
        this.contacts = contacts;
      }
    );
    this.contactsService.emitContacts();

    this.groupsSubscription = this.contactsService.groupsSubject.subscribe(
      (groups: any[]) => {
        this.groups = groups;
      }
    );
    this.contactsService.emitGroups();

    // Instanciation du model CRUD
    this.contactModel = {name:'', id:0, mail:'', city:'',isAdmin:false, photo:'',job:'',phone:'',mobile:'',surname:'', uid: '', groups: this.groups};
    this.resetPassword = false;

    // Appel de la liste de contact
    this.contactsService.getContacts().then(
      () => { },
      (err) => {
        this.handleErrors(err);
      }
    );

    // Etat des filtres
    this.searchAll = false;
    this.switchSelectAll();
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.contactsSubscription.unsubscribe();
  }

  // Critères de recherche : Sélectionner tous/aucuns
  switchSelectAll(){
    this.searchAll = !this.searchAll;
    
    this.searchOptions = {
      onCity: this.searchAll,
      onFunction: this.searchAll,
      onMail: this.searchAll,
      onName: this.searchAll,
      onPhone: this.searchAll,
      onNickName: this.searchAll
    };



    if (this.searchAll){
      this.selectLabel = "Décocher tous";
    }
    else{
      this.selectLabel = "Cocher tous";
    }
  }

  // OPERATIONS CRUD
  // Suppression d'un contact
  onDeleteContactCalled(contact: Contact){
    // Assignment du model au contact sélectionné
    this.contactModel = Object.assign({}, contact);

    document.getElementById("openDeleteModalButton").click();
  }

  onDeleteContactSave(contact: Contact){
    this.contactsService.removeContact(contact)
    .then(
      () => { 
        this.appComponent.createNotification({type: "success", title: "Utilisateur supprimé", content: "L'utilisateur " + this.contactModel.surname + " " + this.contactModel.name + " a correctement été supprimé."});
      },
      (error) => {
        this.handleErrors(error);
      }
    );
  }

  // Lecture d'un contact
  onDetailsContactCalled(contact: Contact) {
    // Assignment du model au contact sélectionné
    this.contactModel = Object.assign({}, contact);
    document.getElementById("openDetailsModalButton").click();
  }

  // Edition d'un contact
  onEditContactCalled(contact: Contact) {
    // Assignment du model au contact sélectionné
    this.contactModel = Object.assign({}, contact);

    document.getElementById("openEditModalButton").click();
  }

  async onEditContactSave(){
    try {
      await this.contactsService.editContact(this.contactModel, this.resetPassword);
    } catch (error) {
      this.handleErrors(error);
      return;
    }
    
    if (this.resetPhoto && this.contactModel.photo != ''){
      try {
        await this.photoService.removePhotoByUid(this.contactModel.uid);
      } catch (error) {
        this.handleErrors(error);
        return;
      }
    }

    try {
      await this.contactsService.getContacts();
    } catch (error) {
      this.handleErrors(error);
      return;
    }

    this.appComponent.createNotification({type: "success", title: "Utilisateur modifié", content: "L'utilisateur " + this.contactModel.surname + " " + this.contactModel.name + " a correctement été modifié."});

    this.resetPassword = false;
    this.resetPhoto = false;
  }

  // Ajout d'un contact
  onNewContactCalled(){
    this.contactModel = {name:'', id:0, mail:'', city:'',isAdmin:false, photo:'',mobile:'', job:'',phone:'',surname:'', uid: '', groups: this.groups};
    document.getElementById("openNewModalButton").click();
  }

  onNewContactSave(){
    this.contactsService.createNewContact(this.contactModel)
    .then(
      () => { 
        this.appComponent.createNotification({type: "success", title: "Utilisateur ajouté", content: "L'utilisateur " + this.contactModel.surname + " " + this.contactModel.name + " a correctement été ajouté."});
      },
      (error) => {
        this.handleErrors(error);
      }
    );
  }

  // Click to call <3
  clickToCall(contact: Contact){
    this.voipService.call(contact.phone).then(
      () => { 
        this.appComponent.createNotification({type: "success", title: "En appel", content: "Vous êtes en correspondance avec " + contact.name + " " + contact.surname + "."});
      },
      (error) => {
        this.handleErrors(error);
      }
    );
  }

  handleErrors(error){
    if (error instanceof HttpErrorResponse && error.status == 401){
      this.appComponent.createNotification({type: "warning", title: "Session expirée", content: "Votre session a expirée, merci de vous reconnecter."});
      this.router.navigate(['/auth']);
    } else {
      this.appComponent.createNotification({type: "danger", title: "Erreur", content: error.error.message});
    }
  }
}
