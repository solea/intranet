// Model User représentant un utilisateur dans le LDAP
export class Contact {
    id: number;
    name: string;
    surname: string;
    mail: string;
    phone: string;
    mobile: string;
    job: string;
    city: string;
    photo: string;
    isAdmin: boolean;
    uid: string;
    groups: any[];
  }