// Model User représentant un utilisateur dans le LDAP
export class FilterOptions {
    onName: boolean;
    onNickName: boolean;
    onFunction: boolean;
    onCity: boolean;
    onPhone: boolean;
    onMail: boolean;
  }