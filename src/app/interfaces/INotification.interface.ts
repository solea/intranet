export interface INotification {
    type: string;
    title: string;
    content: string;
  }