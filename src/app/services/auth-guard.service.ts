import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { AppComponent } from '../app.component';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        
        this.authService.updateUser().then(
          () => { resolve(true); }
          ,(err) => { 
            this.router.navigate(['/auth']);
            reject(false);
          }
        );
      }
    );
  }
}