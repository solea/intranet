import { TestBed } from '@angular/core/testing';

import { VoipService } from './voip.service';

describe('VoipService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VoipService = TestBed.get(VoipService);
    expect(service).toBeTruthy();
  });
});
