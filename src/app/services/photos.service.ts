import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


const httpOptions = {
  withCredentials: true,
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private http: HttpClient) { }

  removePhoto() {

    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}deleteProfil`;

        this.http.delete(apiURL, httpOptions)
          .toPromise()
          .then(
            res => { 
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  removePhotoByUid(uid: string) {

    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}deleteProfil/${uid}`;

        this.http.delete(apiURL, httpOptions)
          .toPromise()
          .then(
            res => {
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  public uploadImage(image: File) {
    const formData = new FormData();

    formData.append('image', image);

    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}upload`;

        this.http.post(apiURL, formData, { withCredentials: true })
          .toPromise()
          .then(
            res => {
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }
}