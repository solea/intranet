import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';
import { ContactService } from 'src/app/services/contact.service';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';


const httpOptions = {
  withCredentials: true,
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class AuthService {

  // Utilisateur actuellement connecté
  loggedUserSubject = new Subject<any>();
  private loggedUser = new Contact();


  constructor(private http: HttpClient) { }

  emitLoggedUser() {
    this.loggedUserSubject.next(this.loggedUser);
  }

  signIn(user: string, password: string) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}auth`;

        this.http.post(apiURL, JSON.stringify({uid: user, password: password}), httpOptions)
          .toPromise()
          .then(
            res => { // Success

              this.loggedUser.id = res["user"].id;
              this.loggedUser.uid = res["user"].uid;
              this.loggedUser.surname = res["user"].surname;
              this.loggedUser.name = res["user"].name;
              this.loggedUser.mail = res["user"].mail;
              this.loggedUser.phone = res["user"].phone;
              this.loggedUser.mobile = res["user"].mobile;
              this.loggedUser.job = res["user"].job;
              this.loggedUser.city = res["user"].location;
              this.loggedUser.isAdmin = res["user"].admin;
              this.loggedUser.groups = res["user"].groups;
              this.loggedUser.photo = res["user"].photo;

              if (this.loggedUser.photo != '') this.loggedUser.photo = environment.pictures + this.loggedUser.photo
              

              resolve();
            },
            err => { // Error

              var myError = {};
              if (err instanceof HttpErrorResponse){
                if (err.status == 0 ) {
                  if (environment.production) {
                    myError['message'] = 'Le serveur d\'authentification est actuellement indisponible. Merci de patienter quelques instants. Rendez-vous sur https://intranet.solea3.fr:5000 afin de corriger l\'erreur.';
                  } else {
                    myError['message'] = 'Le serveur d\'authentification est actuellement indisponible. Merci de patienter quelques instants.';
                  }
                } else { // Erreur API
                  myError['message'] = err.error.message;
                }
              }

              reject(myError);
            }
          );
      });
  }

  signOut() {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}logout`;

        this.http.get(apiURL, httpOptions)
          .toPromise()
          .then(
            res => { // Success
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  updateUser() {
    
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}whoIAm`;

        this.http.get(apiURL, httpOptions)
          .toPromise()
          .then(
            res => { // Success
              this.loggedUser.id = res["user"].id;
              this.loggedUser.uid = res["user"].uid;
              this.loggedUser.surname = res["user"].surname;
              this.loggedUser.name = res["user"].name;
              this.loggedUser.mail = res["user"].mail;
              this.loggedUser.phone = res["user"].phone;
              this.loggedUser.mobile = res["user"].mobile;
              this.loggedUser.job = res["user"].job;
              this.loggedUser.city = res["user"].location;
              this.loggedUser.isAdmin = res["user"].admin;
              this.loggedUser.photo = res["user"].photo;
              this.loggedUser.groups = res["user"].groups;

              if (this.loggedUser.photo != '') this.loggedUser.photo = environment.pictures + this.loggedUser.photo;

              this.emitLoggedUser();

              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }
}