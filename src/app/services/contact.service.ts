import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Contact } from '../models/contact.model';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs/Subscription';
import { environment } from 'src/environments/environment';

const httpOptions = {
  withCredentials: true,
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  user: Contact;
  userSubscription: Subscription;

  // Gère les événements sur les contacts (ajout/suppression/modification)
  contactsSubject = new Subject<any[]>();
  groupsSubject = new Subject<any[]>();

  private contacts = new Array<Contact>();
  private groups = new Array<any>();

  constructor(private http: HttpClient, private authService: AuthService) {
    // Souscriptions
    this.userSubscription = this.authService.loggedUserSubject.subscribe(
      (contact: Contact) => {
        this.user = contact;
      }
    );
    this.authService.emitLoggedUser();
  }

  emitContacts() {
    this.contactsSubject.next(this.contacts);
  }

  emitGroups() {
    this.groupsSubject.next(this.groups);
  }

  getContacts() {

    this.contacts = new Array<Contact>();

    return new Promise(
      async (resolve, reject) => {

        try {
          await this.getGroups();
        } catch (error) {
          reject(error);
        }

        let apiURL = `${environment.api}contacts`;

        this.http.get(apiURL, httpOptions)
          .toPromise()
          .then(
            res => { // Success

              res["contacts"].forEach(contact => {

                if (contact["id"] != this.user.id) {

                  var groupMember = [];
                  this.groups.forEach(group => {
                    if (contact["groups"].find(grp => grp.guid == group.guid)) {
                      groupMember.push({ guid: group.guid, gname: group.gname, isMember: true });
                    } else {
                      groupMember.push({ guid: group.guid, gname: group.gname, isMember: false });
                    }
                  });

                  var photo = contact["photo"]
                  if (photo != '') {
                    photo = environment.pictures + photo;
                  }
                  this.contacts.push({
                    id: contact["id"],
                    surname: contact["surname"],
                    name: contact["name"],
                    mail: contact["mail"],
                    phone: contact["phone"],
                    mobile: contact["mobile"],
                    job: contact["job"],
                    city: contact["location"],
                    isAdmin: contact["admin"],
                    photo: photo,
                    uid: contact["uid"],
                    groups: groupMember
                  });

                }
              });

              this.emitContacts();

              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  getGroups() {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}groups`;

        this.http.get(apiURL, httpOptions)
          .toPromise()
          .then(
            res => { // Success
              this.groups = [];
              res["groups"].forEach(group => {
                if (group["gname"] != "web_adm")
                  this.groups.push({
                    guid: group["guid"],
                    gname: group["gname"]
                  });
              });

              // Emit
              this.emitGroups();

              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  createNewContact(newContact: Contact) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}addContact`;

        this.http.post(apiURL, JSON.stringify({ surname: newContact.surname, name: newContact.name, job: newContact.job, location: newContact.city }), httpOptions)
          .toPromise()
          .then(
            async res => { // Success

              var createdContact = new Contact();

              createdContact.id = res["contact"].id;
              createdContact.uid = res["contact"].uid;
              createdContact.surname = res["contact"].surname;
              createdContact.name = res["contact"].name;
              createdContact.mail = res["contact"].mail;
              createdContact.phone = res["contact"].phone;
              createdContact.mobile = res["contact"].mobile;
              createdContact.job = res["contact"].job;
              createdContact.city = res["contact"].location;
              createdContact.isAdmin = res["contact"].admin;
              createdContact.photo = res["contact"].photo;

              if (createdContact.photo != '') createdContact.photo = environment.pictures + createdContact.photo;

              if (newContact.isAdmin == true) {
                createdContact.isAdmin = true;

                try {
                  await this.grantAdminContact(createdContact);
                } catch (error) {
                  reject(error);
                  return;
                }
              }

              var groupsToAdd = [];
              newContact.groups.forEach(group => {
                if (group.isMember == true) {
                  groupsToAdd.push(group.gname);
                }
              });

              if (groupsToAdd.length > 0) {
                try {
                  await this.addGroupsToContact(createdContact, groupsToAdd);
                }
                catch (error) {
                  reject(error);
                  return;
                }
              }

              try {
                await this.getContacts();
              }
              catch (error) {
                reject(error);
                return;
              }

              resolve();
            }

          );
      });
  }

  removeContact(contact: Contact) {

    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}deleteContact/${contact.uid}`;

        this.http.delete(apiURL, httpOptions)
          .toPromise()
          .then(
            res => { // Success

              this.getContacts().then(
                () => {
                  resolve();
                },
                (err) => {
                  reject(err);
                  return;
                }
              );
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  editContact(contact: Contact, resetPassword: boolean) {
    return new Promise(
      (resolve, reject) => {
        let apiURL = `${environment.api}editContact/${contact.uid}`;

        this.http.post(apiURL, JSON.stringify({ surname: contact.surname, name: contact.name, job: contact.job, location: contact.city }), httpOptions)
          .toPromise()
          .then(
            async res => { // Success

              var editedContact = new Contact();

              editedContact.id = res["contact"].id;
              editedContact.uid = res["contact"].uid;
              editedContact.surname = res["contact"].surname;
              editedContact.name = res["contact"].name;
              editedContact.mail = res["contact"].mail;
              editedContact.phone = res["contact"].phone;
              editedContact.mobile = res["contact"].mobile;
              editedContact.job = res["contact"].job;
              editedContact.city = res["contact"].location;
              editedContact.isAdmin = res["contact"].admin;
              editedContact.photo = res["contact"].photo;

              if (editedContact.photo != '') editedContact.photo = environment.pictures + editedContact.photo;

              var groupMember = [];
              this.groups.forEach(group => {
                if (res["contact"].groups.find(grp => grp.guid == group.guid)) {
                  groupMember.push({ guid: group.guid, gname: group.gname, isMember: true });
                } else {
                  groupMember.push({ guid: group.guid, gname: group.gname, isMember: false });
                }
              });


              // Si on a basculé l'état est admin
              var checkUser = (this.contacts.find(c => c.id == editedContact.id));
              if (checkUser) {
                if (checkUser.isAdmin != contact.isAdmin) {

                  try {
                    await this.grantAdminContact(contact);
                    editedContact.isAdmin = contact.isAdmin;
                  }
                  catch (error) {
                    reject(error);
                    return;
                  }
                }
              }

              if (resetPassword == true) {
                try {
                  await this.resetPassword(contact);
                }
                catch (error) {
                  reject(error);
                  return;
                }
              }

              var groupsToAdd = [];
              contact.groups.forEach(group => {
                if (group.isMember === true) {
                  groupMember.forEach(g => {
                    if (g.guid === group.guid && g.isMember === false) {
                      groupsToAdd.push(group.gname);
                    }
                  });
                }
              });

              if (groupsToAdd.length > 0) {
                try {
                  await this.addGroupsToContact(contact, groupsToAdd);
                }
                catch (error) {
                  reject(error);
                  return;
                }
              }

              var groupsToDelete = [];
              contact.groups.forEach(group => {
                if (group.isMember == false) {
                  groupMember.forEach(g => {
                    if (g.guid == group.guid && g.isMember == true) {
                      groupsToDelete.push(group.gname);
                    }
                  });
                }
              });

              if (groupsToDelete.length > 0) {
                try {
                  await this.deleteContactGroups(contact, groupsToDelete);
                }
                catch (error) {
                  reject(error);
                  return;
                }
              }

              try {
                await this.getContacts();
              }
              catch (error) {
                reject(error);
                return;
              }

              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  grantAdminContact(contact: Contact) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}grantAdmin/${contact.uid}`;

        this.http.post(apiURL, JSON.stringify({ admin: contact.isAdmin }), httpOptions)
          .toPromise()
          .then(
            res => { // Success
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  resetPassword(contact: Contact) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}resetPassword/${contact.uid}`;

        this.http.post(apiURL, null, httpOptions)
          .toPromise()
          .then(
            res => { // Success
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }


  changePassword(oldPassword: string, newPassword: string) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}changePassword`;

        this.http.post(apiURL, JSON.stringify({ oldPassword: oldPassword, newPassword: newPassword }), httpOptions)
          .toPromise()
          .then(
            res => { // Success
              resolve();
            },
            error => { // Error
              console.log(error);
              reject(error);
            }
          );
      });
  }

  addGroupsToContact(contact: Contact, groups: string[]) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}addContactGroups/${contact.uid}`;

        this.http.post(apiURL, JSON.stringify({ groups: groups }), httpOptions)
          .toPromise()
          .then(
            res => { // Success
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

  deleteContactGroups(contact: Contact, groups: string[]) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}deleteContactGroups/${contact.uid}`;

        this.http.post(apiURL, JSON.stringify({ groups: groups }), httpOptions)
          .toPromise()
          .then(
            res => { // Success
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }

}