import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { INotification } from '../interfaces/INotification.interface';
import { NotificationItem } from '../components/notifications/notification-item';
import { NotificationsComponent } from '../components/notifications/notifications.component';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  
  // Gère les événements sur les notifications (ajout/suppression)
  notificationsSubject = new Subject<any[]>();

  private notifications = new Array<NotificationItem>();

  constructor() { }

  emitNotifications() {
    this.notificationsSubject.next(this.notifications);
  }

  createNotification(notification: INotification){
    this.notifications[0] = new NotificationItem(NotificationsComponent, notification);
    this.emitNotifications();
  }

  removeNotification(index: number){
    this.notifications = this.notifications.splice(index, 1);
    this.emitNotifications();
  }

  getNotifications(){
    return this.notifications;
  }
}
