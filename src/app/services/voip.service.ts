import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

const httpOptions = {
  withCredentials: true,
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class VoipService {

  constructor(private http: HttpClient) { }


  call(extension: string) {
    return new Promise(
      (resolve, reject) => {

        let apiURL = `${environment.api}call/${extension}`;

        this.http.post(apiURL, null, httpOptions)
          .toPromise()
          .then(
            res => { // Success
              resolve();
            },
            error => { // Error
              reject(error);
            }
          );
      });
  }
}
