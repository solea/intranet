import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ContactService } from './services/contact.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AnnuaireComponent } from './components/dashboard/annuaire/annuaire.component';
import { ProfilComponent } from './components/dashboard/profil/profil.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ContactNameFilterPipe } from './pipes/contact-name-filter.pipe';
import { LabelFilterPipePipe } from './pipes/label-filter-pipe.pipe';
import { NotificationsDirective } from './directives/notifications.directive';
import { NotificationsComponent } from './components/notifications/notifications.component';

const appRoutes: Routes = [
  { path: 'auth', component: AuthComponent },

  { path: 'dashboard', canActivate: [AuthGuardService], component: DashboardComponent, children:
    [
      { path: 'annuaire', component: AnnuaireComponent },
      { path: 'profil', component: ProfilComponent },
      { path: '', component: AnnuaireComponent },
    ] 
  },

  { path: '', redirectTo: 'dashboard/annuaire', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AnnuaireComponent,
    ProfilComponent,
    AuthComponent,
    NotFoundComponent,
    ContactNameFilterPipe,
    LabelFilterPipePipe,
    NotificationsDirective,
    NotificationsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    ContactService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    NotificationsComponent
  ]
})
export class AppModule { }
