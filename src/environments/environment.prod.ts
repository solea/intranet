export const environment = {
  production: true,
  api: "https://intranet.solea3.fr:5000/api/v1/",
  pictures: "https://intranet.solea3.fr:5000/"
};
