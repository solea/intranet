# Annuaire

Ce dépôt contient la partie *Frontend* de l'interface de gestion du LDAP basé sous Angular 7 et Bootsrap 4 pour le visuel.


## Compiler l'interface

Exécutez `ng serve`. Rendez-vous à l'url http://localhost:4200/ avec votre navigateur favoris. L'application se recharge automatiquement à mesure que vous éditez les fichiers sources.

## Générer l'interface

Exécutez `ng build` pour déployer l'interface. Les fichiers nouvellement générés se trouveront dans le répertoire `dist/`. Utilisez le paramètre `--configuration=production` pour un déploiement en production.
